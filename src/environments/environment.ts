// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyAToF_g-kYeAkjXrYFtUIWUEhs1bG0pcII",
    authDomain: "webboard-c575e.firebaseapp.com",
    databaseURL: "https://webboard-c575e.firebaseio.com",
    projectId: "webboard-c575e",
    storageBucket: "webboard-c575e.appspot.com",
    messagingSenderId: "828978984069",
    appId: "1:828978984069:web:6d60a6315cad7993e365ec"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
