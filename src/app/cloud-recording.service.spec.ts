import { TestBed } from '@angular/core/testing';

import { CloudRecordingService } from './cloud-recording.service';

describe('CloudRecordingService', () => {
  let service: CloudRecordingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CloudRecordingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
