import { ParseTreeResult } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

import { NgxAgoraService, Stream, AgoraClient, ClientEvent, ClientConfig, StreamSpec, StreamEvent } from 'ngx-agora';

import { CloudRecordingService } from './cloud-recording.service';

var config: ClientConfig = {
  mode: "live",
  codec: "vp8",
};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'agora';
  localCallId = 'agora_local';
  remoteCalls: string[] = [];
  // remoteCalls: string[] = [];
  private client: any;
  private screenClient: any;
  private screenStream: any;
  private localStream: any;
  private uid: any;
  private sid: any;

  screenShareBtn = false;



  constructor(public ngxAgoraService: NgxAgoraService, public cloudRecording: CloudRecordingService) {
    this.uid = Math.floor(Math.random() * 100);
    this.cloudRecording.uid = this.uid;
  }

  ngOnInit() {
    // this.client = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });

    // this.assignClientHandlers();
    // this.localStream = this.ngxAgoraService.createStream({ streamID: this.uid, audio: true, video: true});
    
    // this.assignLocalStreamHandlers();
    // this.initLocalStream(() => this.join(uid => this.publish(), error => console.log(error)));

    // this.cloudRecording.acquireResourceId();
  }

  screenShare(): void{
    this.screenClient = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
    
    this.sid = new Date().getUTCMilliseconds();
    console.log(this.sid);
    this.screenClient.on(ClientEvent.LocalStreamPublished, (evt: any) => {
      console.log('Publish screen stream successfully');
    });

    this.screenStream = this.ngxAgoraService.createStream({ streamID: this.sid , audio: false, video: false, screen: true, screenAudio: false })

    this.assignScreenStreamHandlers();
    this.initScreenShare(() => this.joinScreenShare(uid => this.publishScreeShare(), error => console.log(error)));
  }


  join(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
    this.client.join('006e79d64022bde4fb8a5891c994bd0c705IABA/G3UeJ3iwPlSi6jWQNkqZfcIJyBEgqR4oGi/I3PwsH6v0bwAAAAAEAAm+nFWeWXXYAEAAQB4Zddg', '007', this.uid, onSuccess, onFailure);
  }

  joinScreenShare(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
    this.screenClient.join('006e79d64022bde4fb8a5891c994bd0c705IABA/G3UeJ3iwPlSi6jWQNkqZfcIJyBEgqR4oGi/I3PwsH6v0bwAAAAAEAAm+nFWeWXXYAEAAQB4Zddg', '007', this.sid, onSuccess, onFailure);
  }

  publishScreeShare(): void {
    this.screenClient.publish(this.screenStream, (err: any) => console.log('Publish screen stream error: ' + err));
    this.screenShareBtn = true;
  }

  publish(): void {
    this.client.publish(this.localStream, (err: any) => console.log('Publish local stream error: ' + err));
  }

  unpublishScreenShare(): void {
    this.screenClient.unpublish(this.screenStream, (err: any) => console.log('Publish screen stream error: ' + err));
    // this.screenStream.
    this.screenShareBtn = false;
  }

  private assignClientHandlers(): void {
    this.client.on(ClientEvent.LocalStreamPublished, (evt: any) => {
      console.log('Publish local stream successfully');
    });

    this.client.on(ClientEvent.Error, (error: any) => {
      console.log('Got error msg:', error.reason);
      if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.client.renewChannelKey(
          '',
          () => console.log('Renewed the channel key successfully.'),
          (renewError: any) => console.error('Renew channel key failed: ', renewError)
        );
      }
    });


    this.client.on(ClientEvent.RemoteStreamAdded, (evt: any) => {
      console.log("Joineddddd");
      const stream = evt.stream as Stream;
      this.client.subscribe(stream, (err: any) => {
        console.log('Subscribe stream failed', err);
      });
    });


    this.client.on(ClientEvent.RemoteStreamSubscribed, (evt: any) => {
      const stream = evt.stream as Stream;
      console.log(stream);
      const id = this.getRemoteId(stream);
      console.log("From remote client");
      // if (!this.remoteCalls.length) {
        this.remoteCalls.push(id);
        console.log(this.remoteCalls);
        setTimeout(() => stream.play(id), 1000);
      // }
      console.log(this.remoteCalls)
    });



    this.client.on(ClientEvent.RemoteStreamRemoved, (evt: any) => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        console.log(this.remoteCalls)
        var clientName = "agora_remote-" + stream.getId();
        this.remoteCalls = this.remoteCalls.filter(function(val,index,arr) {
          return val != clientName
        })
        console.log(`Remote stream is removed ${stream.getId()}`);
      }
    });


    this.client.on(ClientEvent.PeerLeave, (evt: any) => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
        console.log(`${evt.uid} left from this channel`);
      }
    });


  }

  private assignLocalStreamHandlers(): void {
    this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
    });

    // The user has denied access to the camera and mic.
    this.localStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
    });
  }

  private assignScreenStreamHandlers(): void {
    // The user has denied access to the camera and mic.
    this.screenStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
    });

    this.screenStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
    });
  }

  private initLocalStream(onSuccess?: () => any): void {
    this.localStream.init(
      () => {
        // The user has granted access to the camera and mic.
        this.localStream.play(this.localCallId);
        if (onSuccess) {
          onSuccess();
        }
      },
      (err: any) => console.error('getUserMedia failed', err)
    );

  }

  public initScreenShare(onSuccess?: () => any): void {
    this.screenStream.init(
      () => {
        // The user has granted access to the camera and mic.
        this.screenStream.play(this.localCallId);
        if (onSuccess) {
          onSuccess();
        }
      },
      (err: any) => console.error('getUserMedia failed', err)
    );
  }

  private getRemoteId(stream: Stream): string {
    return `agora_remote-${stream.getId()}`;
  }

}
