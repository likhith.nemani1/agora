import { TestBed } from '@angular/core/testing';

import { ShapeNodeService } from './shape-node.service';

describe('ShapeNodeService', () => {
  let service: ShapeNodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShapeNodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
