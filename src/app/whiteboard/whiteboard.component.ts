import { Component, OnInit,HostListener } from '@angular/core';
import Konva from 'konva';
import { ShapeNodeService } from './services/shape-node.service';
import { TextNodeService } from './services/text-node.service';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { fabric } from 'fabric';


@Component({
  selector: 'app-whiteboard',
  templateUrl: './whiteboard.component.html',
  styleUrls: ['./whiteboard.component.css']
})
export class WhiteboardComponent implements OnInit {

  canvas: any;
  json: any;
  objectSelected = null;

  items: any;
  data: any = "";

  shapes: any = [];
  stage: Konva.Stage | any;
  layer: Konva.Layer | any;
  selectedButton: any = {
    'circle': false,
    'rectangle': false,
    'line': false,
    'undo': false,
    'erase': false,
    'text': false
  }
  erase: boolean = false;
  transformers: Konva.Transformer[] = [];
  clickStartShape: any;
  constructor(
    private shapeService: ShapeNodeService,
    private textNodeService: TextNodeService,
    private db: AngularFireDatabase
  ) { }
  ngOnInit() {
    // let width = window.innerWidth * 0.5;
    // let height = window.innerHeight * 0.5;
    // this.stage = new Konva.Stage({
    //   container: 'container',
    //   width: width,
    //   height: height
    // });
    // this.layer = new Konva.Layer();
    // this.stage.add(this.layer);
    // this.addLineListeners();
    // this.getItems();
    this.canvas = new fabric.Canvas('myCanvas');
    // this.canvas.add(new fabric.IText('Hello Fabric!'));
    let container = this;
    this.canvas.on('mouse:down', function (e: any) {
      console.log('mouse down' + e);
      container.saveJSON();
      // container.objectSelected = null;
    })
    this.canvas.on('mouse:up', function (e: any) {
      console.log('mouse up' + e);
      container.saveJSON();
      // container.objectSelected = e;
    })
    // this.canvas.on('mouse:over', function (e: any) {
    //   container.objectSelected = e;
    // })
    // this.canvas.on('mouse:out', function(e: any) {
    //   container.objectSelected = null;
    // })
    // this.canvas.on('object:modified', function (e: any) {
    //   console.log('object modified');
    //   container.objectSelected = null;
    // })
    this.loadJSON();
  }

  // @HostListener('window:keyup', ['$event'])
  // keyEvent(event: KeyboardEvent) {
  //   this.saveJSON();
  // }

  clearCanvas(): any {
    this.canvas.clear();
    this.saveJSON();
  }

  addTextField(): any {
    this.canvas.isDrawingMode = false;
    this.canvas.add(new fabric.IText("Enter here"));
  }

  addPaintBrush(): any {
    this.canvas.isDrawingMode = true;
  }

  addRectangle(): any {
    this.canvas.add(new fabric.Rect({
      width: 200,
      height: 100,
      fill: '',
      stroke: 'red',
      strokeWidth: 3
    }));
  }

  addCircle(): any {
    this.canvas.add(new fabric.Circle({
      radius: 50,
      stroke: 'green',
      strokeWidth: 3,
      fill: ''
    }))
  }

  saveJSON(): any {
    console.log(this.canvas.getObjects())
    this.db.object('/whiteboard').set(JSON.stringify(this.canvas));
  }

  undoCanvas(): any {
    let x = this.canvas.getObjects();
    this.canvas.remove(x[x.length - 1]);
    this.saveJSON();
  }

  loadJSON(): any {
    let comp = this;
    this.db.object('/whiteboard').valueChanges().subscribe((e: any) => {
      this.items = e;
      if (e != '') {
        this.canvas.loadFromJSON(JSON.parse(e), function () {
          comp.canvas.renderAll();
          // console.log(comp.canvas.item(0).name);
        });
      }
    })
  }


  getItems() {
    this.db.object('/testing').valueChanges().subscribe((e: any) => {
      this.items = e;
      if (e != '') {
        this.stage = Konva.Node.create(e, 'container');
      }
    })
  }

  updateItem(): any {
    this.db.object('/testing').set(this.stage.toJSON());
  }



  clearSelection() {
    Object.keys(this.selectedButton).forEach(key => {
      this.selectedButton[key] = false;
    })
  }
  setSelection(type: string) {
    this.selectedButton[type] = true;
  }
  // addShape(type: string) {
  //   this.clearSelection();
  //   this.setSelection(type);
  //   if (type == 'circle') {
  //     this.addCircle();
  //   }
  //   else if (type == 'line') {
  //     this.addLine();
  //   }
  //   else if (type == 'rectangle') {
  //     this.addRectangle();
  //   }
  //   else if (type == 'text') {
  //     this.addText();
  //   }
  // }
  // addText() {
  //   const text = this.textNodeService.textNode(this.stage, this.layer);
  //   this.shapes.push(text.textNode);
  //   this.transformers.push(text.tr);
  //   // this.updateItem();
  // }
  // addCircle() {
  //   const circle = this.shapeService.circle();
  //   this.shapes.push(circle);
  //   this.layer.add(circle);
  //   this.stage.add(this.layer);
  //   this.addTransformerListeners();
  //   // this.updateItem();
  // }
  // addRectangle() {
  //   const rectangle = this.shapeService.rectangle();
  //   this.shapes.push(rectangle);
  //   this.layer.add(rectangle);
  //   this.stage.add(this.layer);
  //   this.addTransformerListeners()
  // }
  addLine() {
    this.selectedButton['line'] = true;
  }
  addLineListeners() {
    const component = this;
    let lastLine: any;
    let isPaint: any;
    this.stage.on('mousedown touchstart', function () {
      if (!component.selectedButton['line'] && !component.erase) {
        return;
      }
      isPaint = true;
      let pos = component.stage.getPointerPosition();
      const mode = component.erase ? 'erase' : 'brush';
      lastLine = component.shapeService.line(pos, mode)
      component.shapes.push(lastLine);
      component.layer.add(lastLine);
      // component.updateItem();
    });
    this.stage.on('mouseup touchend', function () {
      isPaint = false;
      // component.updateItem();
    });
    // and core function - drawing
    this.stage.on('mousemove touchmove', function () {
      if (!isPaint) {
        return;
      }
      const pos = component.stage.getPointerPosition();
      var newPoints = lastLine.points().concat([pos.x, pos.y]);
      lastLine.points(newPoints);
      component.layer.batchDraw();
      // component.updateItem();
    });
  }
  undo() {
    const removedShape = this.shapes.pop();
    this.transformers.forEach(t => {
      t.detach();
    });
    if (removedShape) {
      removedShape.remove();
    }
    this.layer.draw();
    // let component = this;
    // component.updateItem();
  }
  addTransformerListeners() {
    const component = this;
    const tr = new Konva.Transformer();
    this.stage.on('click', (e: any) => {
      if (!this.clickStartShape) {
        return;
      }
      if (e.target._id == this.clickStartShape._id) {
        component.addDeleteListener(e.target);
        component.layer.add(tr);
        tr.attachTo(e.target);
        component.transformers.push(tr);
        component.layer.draw();
      }
      else {
        tr.detach();
        component.layer.draw();
      }
    });
  }
  addDeleteListener(shape: any) {
    const component = this;
    window.addEventListener('keydown', function (e) {
      if (e.keyCode === 46) {
        shape.remove();
        component.transformers.forEach(t => {
          t.detach();
        });
        const selectedShape = component.shapes.find((s: any) => s._id == shape._id);
        selectedShape.remove();
        e.preventDefault();
      }
      component.layer.batchDraw();
    });
  }
}

export class Item {
  test: string | undefined;
}