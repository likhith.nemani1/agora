import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CloudRecordingService {

  AppID = 'e79d64022bde4fb8a5891c994bd0c705';

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa('b3da1e6ca41b4eff9c5bd37a4627405d:0df31c7bccb445b3aa2a597bfee6ef9d')
    }),
  };

  resourceId: string = "";

  sid: string = "";

  uid: string = "";

  constructor(public httpClient: HttpClient) { }

  acquireResourceId(): void {
    var url = `https://api.agora.io/v1/apps/${this.AppID}/cloud_recording/acquire`;
    console.log(this.uid + 2 + "");
    var body = {
      "cname": "007",
      "uid": this.uid + 2 + "",
      "clientRequest":{
        "resourceExpiredHour": 24,
        "scene": 2
      }
    }
    this.httpClient.post(url, body, this.httpHeader).subscribe(
      (res: any) => {
        console.log(res);
        this.resourceId = res.resourceId;
        this.startRecording();
      },
      (err: any) => {
        console.log(err);
      }
    )
  }

  startRecording(): void {

    // var body = {
    //   "uid": this.uid + 2 + "",
    //   "cname": "007",
    //   "clientRequest": {
    //     "token": '006e79d64022bde4fb8a5891c994bd0c705IABA/G3UeJ3iwPlSi6jWQNkqZfcIJyBEgqR4oGi/I3PwsH6v0bwAAAAAEAAm+nFWeWXXYAEAAQB4Zddg',
    //     "recordingConfig": {
    //       "channelType": 0,
    //       "streamTypes": 2,
    //       "audioProfile": 1,
    //       "videoStreamType": 0,
    //       "maxIdleTime": 120,
    //       "transcodingConfig": {
    //         "width": 320,
    //         "height": 640,
    //         "fps": 30,
    //         "bitrate": 600,
    //         "maxResolutionUid": "1",
    //         "mixedVideoLayout": 1
    //       }
    //     },
    //     "storageConfig": {
    //       "vendor": 1,
    //       "region": 14,
    //       "bucket": "cloudrecordingdemo",
    //       "accessKey": "AKIA4N5HZQCU7U7QLP65",
    //       "secretKey": "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"
    //     }
    //   }
    // }

    var body = {
      "uid": this.uid + 2 + "",
      "cname": "007",
      "clientRequest": {
        "token": "006e79d64022bde4fb8a5891c994bd0c705IABlClXcRL1cWKJfu9SzN3j74fQ5F42gLXs9XlAcgXbwwX6v0bwAAAAAEAA7+TVQV4QBYQEAAQBWhAFh",
        "appsCollection": {
          "combinationPolicy": "postpone_transcoding"
        },
        // "recordingConfig": {
        //   "maxIdleTime": 30,
        //   "streamTypes": 2,
        //   "channelType": 0,
        //   "videoStreamType": 1,
        //   "subscribeVideoUids": ["123", "456"],
        //   "subscribeAudioUids": ["123", "456"],
        //   "subscribeUidGroup": 0
        // },
        "storageConfig": {
          "vendor": 1,
          "region": 14,
          "bucket": "cloudrecordingdemo",
          "accessKey": "AKIA4N5HZQCU7U7QLP65",
          "secretKey": "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"
        }
      }
    }

    var url = `https://api.agora.io/v1/apps/${this.AppID}/cloud_recording/resourceid/${this.resourceId}/mode/individual/start`;
    this.httpClient.post(url, body, this.httpHeader).subscribe(
      (res: any) => {
        console.log(res);
        this.sid = res.sid;
      },
      (err: any) => {
        console.log(err);
      }
    )
  }

  stopRecording(): void {
    var body = {
      "cname": "007",
      "uid": this.uid + 2 + "",
      "clientRequest": {
        "async_stop": false
      },
    }
    var url = `https://api.agora.io/v1/apps/${this.AppID}/cloud_recording/resourceid/${this.resourceId}/sid/${this.sid}/mode/individual/stop`;
    this.httpClient.post(url, body, this.httpHeader).subscribe(
      (res: any) => {
        console.log(res);
      },
      (err: any) => {
        console.log(err);
      }
    )
  }


  checkStatus(): void {
    var url = `https://api.agora.io/v1/apps/${this.AppID}/cloud_recording/resourceid/${this.resourceId}/sid/${this.sid}/mode/individual/query`;
    this.httpClient.get(url, this.httpHeader).subscribe(
      (res: any) => {
        console.log(res);
      },
      (err: any) => {
        console.log(err);
      }
    )
  }

}
