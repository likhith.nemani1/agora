import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';


@Component({
  selector: 'app-apollo-angular-demo',
  templateUrl: './apollo-angular-demo.component.html',
  styleUrls: ['./apollo-angular-demo.component.css']
})
export class ApolloAngularDemoComponent implements OnInit {

  data: any[] | undefined;
  loading = true;
  error: any;

  constructor(public apollo: Apollo) { }

  ngOnInit(): void {
    this.getAllPosts();
  }

  getAllPosts(): void {
    this.apollo
      .watchQuery({
        query: gql`query (
        $options: PageQueryOptions
      ) {
        posts(options: $options) {
          data {
            id
            title
          }
          meta {
            totalCount
          }
        }
      }`,
        variables: {
          "options": {
            "paginate": {
              "page": 1,
              "limit": 5
            }
          }
        }
      })
      .valueChanges.subscribe((result: any) => {
        console.log(result);
        this.data = result?.data?.rates;
        this.loading = result.loading;
        this.error = result.error;
      });
  }

  getPost(): void {
    this.apollo
      .watchQuery({
        query: gql`
      {
        post(id: 1) {
          id
          title
          body
        }
      }
    `,
      })
      .valueChanges.subscribe((result: any) => {
        console.log(result);
        this.data = result?.data?.rates;
        this.loading = result.loading;
        this.error = result.error;
      });
  }

  createPost(): void {
    this.apollo
      .mutate({
        mutation: gql`mutation (
        $input: CreatePostInput!
      ) {
        createPost(input: $input) {
          id
          title
          body
        }
      }`,
        variables: {
          "input": {
            "title": "A Very Captivating Post Title",
            "body": "Some interesting content."
          }
        }
      })
      .subscribe((result: any) => {
        console.log(result);
        this.data = result?.data?.rates;
        this.loading = result.loading;
        this.error = result.error;
      });
  }

  updatePost(): void {
    this.apollo
      .mutate({
        mutation: gql`mutation (
          $id: ID!,
          $input: UpdatePostInput!
        ) {
          updatePost(id: $id, input: $input) {
            id
            body
          }
        }`,
        variables: {
          "id": 1,
          "input": {
            "body": "Some updated content."
          }
        }
      })
      .subscribe((result: any) => {
        console.log(result);
        this.data = result?.data?.rates;
        this.loading = result.loading;
        this.error = result.error;
      });
  }

  deletePost(): void {
    this.apollo
      .mutate({
        mutation: gql`mutation (
          $id: ID!
        ) {
          deletePost(id: $id)
        }`,
        variables: {
          "id": 101
        }
      })
      .subscribe((result: any) => {
        console.log(result);
        this.data = result?.data?.rates;
        this.loading = result.loading;
        this.error = result.error;
      });
  }
}
