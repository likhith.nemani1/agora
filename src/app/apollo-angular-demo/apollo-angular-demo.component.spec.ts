import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApolloAngularDemoComponent } from './apollo-angular-demo.component';

describe('ApolloAngularDemoComponent', () => {
  let component: ApolloAngularDemoComponent;
  let fixture: ComponentFixture<ApolloAngularDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApolloAngularDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApolloAngularDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
