import { AfterViewInit, Component, OnInit } from '@angular/core';
import { fabric } from 'fabric';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-canvas-board',
  templateUrl: './canvas-board.component.html',
  styleUrls: ['./canvas-board.component.css']
})
export class CanvasBoardComponent implements OnInit, AfterViewInit {

  canvas: any;
  items: any;

  constructor(public db: AngularFireDatabase) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.canvas = new fabric.Canvas('myCanvas');
    this.canvas.setDimensions({ height: 500, width: 700 });
    let container = this;
    this.canvas.on('mouse:down', function (e: any) {
      console.log('mouse down' + e);
      container.saveJSON();
    });
    this.canvas.on('mouse:up', function (e: any) {
      console.log('mouse up' + e);
      container.saveJSON();
    })
    this.loadJSON();
  }

  loadCanvas(count: number): any {
    if (count == 0) {
      this.canvas = new fabric.Canvas('myCanvas');
      this.canvas.setDimensions({ height: 500, width: 700 });
      let container = this;
      this.canvas.on('mouse:down', function (e: any) {
        console.log('mouse down' + e);
        container.saveJSON();
      });
      this.canvas.on('mouse:up', function (e: any) {
        console.log('mouse up' + e);
        container.saveJSON();
      })
      this.loadJSON();
    } else {
      this.loadCanvas(count - 1);
    }
  }

  clearCanvas(): any {
    this.canvas.clear();
    this.saveJSON();
  }

  addTextField(): any {
    this.canvas.isDrawingMode = false;
    this.canvas.add(new fabric.IText("Enter here"));
  }

  addPaintBrush(): any {
    this.canvas.isDrawingMode = true;
  }

  addRectangle(): any {
    this.canvas.add(new fabric.Rect({
      width: 200,
      height: 100,
      fill: '',
      stroke: 'red',
      strokeWidth: 3
    }));
  }

  addCircle(): any {
    this.canvas.add(new fabric.Circle({
      radius: 50,
      stroke: 'green',
      strokeWidth: 3,
      fill: ''
    }))
  }

  saveJSON(): any {
    console.log(this.canvas.getObjects())
    this.db.object('/whiteboard').set(JSON.stringify(this.canvas));
  }

  undoCanvas(): any {
    let x = this.canvas.getObjects();
    this.canvas.remove(x[x.length - 1]);
    this.saveJSON();
  }

  loadJSON(): any {
    let comp = this;
    this.db.object('/whiteboard').valueChanges().subscribe((e: any) => {
      this.items = e;
      if (e != '') {
        this.canvas.loadFromJSON(JSON.parse(e), function () {
          comp.canvas.renderAll();
        });
      }
    })
  }

}
