import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-rtm',
  templateUrl: './rtm.component.html',
  styleUrls: ['./rtm.component.css']
})
export class RtmComponent implements OnInit {
  
  messages: Message | any = [];
  currentMessage = "";
  id: any;
  name:any = "";
  constructor(private db: AngularFireDatabase) { }

  ngOnInit(): void {
    this.id = Math.floor(Math.random() * 100);
    this.name = prompt("Enter the name","");
    this.getMessages();
  }

  getMessages(): any {
    this.db.object('/rtm').valueChanges().subscribe((e: any) => {
      this.messages = JSON.parse(e);
      console.log(this.messages);
      console.log(e);
    })
  }


  sendMessage(): any {
    let messgObj: Message = {
      id: this.id,
      name: this.name,
      message: this.currentMessage,
      timeStamp: Date.now()
    }
    this.messages.push(messgObj);
    let messgStr = JSON.stringify(this.messages);
    this.db.object('/rtm').set(messgStr);
    this.currentMessage = "";
  }

  clearMessages(): any {
    this.db.object('/rtm').set("[]");
  }

}


export class Message {
  id: any;
  name: String | any;
  message: string | any;
  timeStamp: any;
}