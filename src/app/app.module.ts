import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgxAgoraModule, AgoraConfig } from 'ngx-agora';

import { HttpClientModule } from '@angular/common/http';
import { TextNodeService } from './whiteboard/services/text-node.service';
import { ShapeNodeService } from './whiteboard/services/shape-node.service';
import { WhiteboardComponent } from './whiteboard/whiteboard.component';


import { environment } from "../environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { FormsModule } from '@angular/forms';
import { RtmComponent } from './rtm/rtm.component';
import { CodeEditorModule } from '@ngstack/code-editor';
import { CodeEditorComponent } from './code-editor/code-editor.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { CanvasBoardComponent } from './canvas-board/canvas-board.component';
import { LocalStreamComponent } from './video-call/local-stream/local-stream.component';
import { RemoteStreamComponent } from './video-call/remote-stream/remote-stream.component';
import { ApolloAngularDemoComponent } from './apollo-angular-demo/apollo-angular-demo.component';
import { GraphQLModule } from './graphql.module';



const agoraConfig: AgoraConfig = {
  AppID: 'e79d64022bde4fb8a5891c994bd0c705',
};

@NgModule({
  declarations: [
    AppComponent,
    WhiteboardComponent,
    RtmComponent,
    CodeEditorComponent,
    DashboardComponent,
    HomeComponent,
    CanvasBoardComponent,
    LocalStreamComponent,
    RemoteStreamComponent,
    ApolloAngularDemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxAgoraModule.forRoot(agoraConfig),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    CodeEditorModule.forRoot(),
    GraphQLModule

  ],
  providers: [
    TextNodeService,
    ShapeNodeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
