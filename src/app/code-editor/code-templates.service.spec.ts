import { TestBed } from '@angular/core/testing';

import { CodeTemplatesService } from './code-templates.service';

describe('CodeTemplatesService', () => {
  let service: CodeTemplatesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CodeTemplatesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
