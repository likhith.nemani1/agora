import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CodeTemplatesService } from './code-templates.service';

@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.css']
})
export class CodeEditorComponent implements OnInit {

  theme = 'vs-dark';

  language: any;

  codeModel: CodeModel = {
    language: 'c',
    uri: 'main.c',
    value: '',
    dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
  };

  outputModel: CodeModel = {
    language: 'plaintext',
    uri: 'main.txt',
    value: '',
    // dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
  };

  inputModel: CodeModel = {
    language: 'plaintext',
    uri: 'output.txt',
    value: '',
    // dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
  };

  options = {
    contextmenu: true,
    minimap: {
      enabled: false
    }
  };

  code = "";
  compileError = "";
  currentLanguageId: any;
  prevLangId: any;

  modes = [
    { id: 45, mode: 'UNKNOWN', uri: 'main.asm', name: 'Assembly (NASM 2.14.02)', template: this.codeTemplates.assemblySource },
    { id: 46, mode: 'shell', uri: 'main.sh', name: 'Bash (5.0.0)',template: this.codeTemplates.bashSource },
    { id: 47, mode: 'UNKNOWN', uri: 'main.bas', name: 'Basic (FBC 1.07.1)', template: this.codeTemplates.basicSource },
    // { id: 1011, mode: 'UNKNOWN', uri: 'main.bsq', name: 'Bosque (latest)', template: this.codeTemplates.bosqueSource },
    { id: 75, mode: 'c', uri: 'main.c', name: 'C (Clang 7.0.1)', template: this.codeTemplates.cSource },
    // { id: 1013, mode: 'c', uri: 'main.c', name: 'C (Clang 9.0.1)', template: this.codeTemplates.cSource },
    // { id: 1001, mode: 'c', uri: 'main.c', name: 'C (Clang 10.0.1)', template: this.codeTemplates.cSource },
    { id: 48, mode: 'c', uri: 'main.c', name: 'C (GCC 7.4.0)', template: this.codeTemplates.cSource },
    { id: 49, mode: 'c', uri: 'main.c', name: 'C (GCC 8.3.0)', template: this.codeTemplates.cSource},
    { id: 50, mode: 'c', uri: 'main.c', name: 'C (GCC 9.2.0)', template: this.codeTemplates.cSource },
    { id: 51, mode: 'csharp', uri: 'main.cs', name: 'C# (Mono 6.6.0.161)', template: this.codeTemplates.csharpSource },
    // { id: 1022, mode: 'csharp', uri: 'main.cs', name: 'C# (Mono 6.10.0.104)', template: this.codeTemplates.csharpSource },
    // { id: 1021, mode: 'csharp', uri: 'main.cs', name: 'C# (.NET Core SDK 3.1.302)', template: this.codeTemplates.csharpSource },
    // { id: 1023, mode: 'csharp', uri: 'main.cs', name: 'C# Test (.NET Core SDK 3.1.302, NUnit 3.12.0)', template: this.codeTemplates.csharpTestSource },
    { id: 76, mode: 'cpp', uri: 'main.cpp', name: 'C++ (Clang 7.0.1)', template: this.codeTemplates.cppSource },
    // { id: 1014, mode: 'cpp', uri: 'main.cpp', name: 'C++ (Clang 9.0.1)', template: this.codeTemplates.cppSource },
    // { id: 1002, mode: 'cpp', uri: 'main.cpp', name: 'C++ (Clang 10.0.1)', template: this.codeTemplates.cppSource },
    { id: 52, mode: 'cpp', uri: 'main.cpp', name: 'C++ (GCC 7.4.0)', template: this.codeTemplates.cppSource },
    { id: 53, mode: 'cpp', uri: 'main.cpp', name: 'C++ (GCC 8.3.0)', template: this.codeTemplates.cppSource },
    { id: 54, mode: 'cpp', uri: 'main.cpp', name: 'C++ (GCC 9.2.0)', template: this.codeTemplates.cppSource },
    { id: 60, mode: 'go', uri: 'main.go', name: 'Go (1.13.5)', template: this.codeTemplates.goSource },
    { id: 62, mode: 'java', uri: 'main.java', name: 'Java (OpenJDK 13.0.1)', template: this.codeTemplates.javaSource },
    // { id: 1004, mode: 'java', uri: 'main.java', name: 'Java (OpenJDK 14.0.1)', template: this.codeTemplates.javaSource },
    // { id: 1005, mode: 'java', uri: 'main.java', name: 'Java Test (OpenJDK 14.0.1, JUnit Platform Console Standalone 1.6.2)', template: this.codeTemplates.javaTestSource},
    { id: 63, mode: 'javascript', uri: 'main.js', name: 'JavaScript (Node.js 12.14.0)', template: this.codeTemplates.javaScriptSource },
    { id: 78, mode: 'kotlin', uri: 'main.kt', name: 'Kotlin (1.3.70)', template: this.codeTemplates.kotlinSource },
    { id: 67, mode: 'pascal', uri: 'main.pas', name: 'Pascal (FPC 3.0.4)', template: this.codeTemplates.pascalSource },
    { id: 85, mode: 'perl', uri: 'main.pl', name: 'Perl (5.28.1)', template: this.codeTemplates.perlSource },
    { id: 68, mode: 'php', uri: 'main.php', name: 'PHP (7.4.1)', template: this.codeTemplates.phpSource },
    { id: 43, mode: 'plaintext', uri: 'main.txt', name: 'Plain Text', template: this.codeTemplates.plainTextSource },
    { id: 70, mode: 'python', uri: 'main.py', name: 'Python (2.7.17)', template: this.codeTemplates.pythonSource },
    { id: 71, mode: 'python', uri: 'main.py', name: 'Python (3.8.1)', template: this.codeTemplates.pythonSource },
    // { id: 1010, mode: 'python', uri: 'main.py', name: 'Python for ML (3.7.3)', template: this.codeTemplates.pythonForMlSource },
    { id: 80, mode: 'r', uri: 'main.r', name: 'R (4.0.0)', template: this.codeTemplates.rSource },
    { id: 72, mode: 'ruby', uri: 'main.rb', name: 'Ruby (2.7.0)', template: this.codeTemplates.rubySource },
    { id: 73, mode: 'rust', uri: 'main.rs', name: 'Rust (1.40.0)', template: this.codeTemplates.rustSource },
    { id: 82, mode: 'sql', uri: 'main.sql', name: 'SQL (SQLite 3.27.2)', template: this.codeTemplates.sqliteSource },
    { id: 83, mode: 'swift', uri: 'main.swift', name: 'Swift (5.2.3)', template: this.codeTemplates.swiftSource },
    { id: 74, mode: 'typescript', uri: 'main.ts', name: 'TypeScript (3.7.4)', template: this.codeTemplates.typescriptSource },
    { id: 84, mode: 'vb', uri: 'main.vb', name: 'Visual Basic.Net (vbnc 0.0.0.5943)', template: this.codeTemplates.vbSource },
  ]


  constructor(public http: HttpClient, public codeTemplates: CodeTemplatesService) { }

  ngOnInit(): void {
    this.currentLanguageId = 50;
    this.prevLangId = 50;
    this.codeModel = {
      language: 'c',
      uri: 'main.c',
      value: this.codeTemplates.cSource,
      dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
    }
  }

  onCodeChanged(value: any) {
    this.code = value;
  }

  onLanguageChange(event: any): any {
    if (this.currentLanguageId !== this.prevLangId) {
      console.log("Hello")
      let y = this.modes.find((e) => e.id == this.prevLangId);
      if (y?.template !== this.codeModel.value) {
        if (confirm("The previous code will be erased. Do you wish to continue ?")) {
          this.changeLanguage();
        } else {
          console.log('dfghj')
          console.log(event.target.value);
          this.currentLanguageId = this.prevLangId;
          console.log(this.currentLanguageId);
        }
      } else {
        this.changeLanguage();
      }
    }
  }

  changeLanguage(): any {
    let x = this.modes.find((e) => e.id == this.currentLanguageId);
    let val = this.codeModel.value;
    if (x) {
      this.codeModel = {
        language: x.mode,
        uri: x.uri,
        value: x.template
      }
    }
    this.prevLangId = this.currentLanguageId;
    console.log(this.codeModel);
  }

  generateToken(): any {
    let body = {
      "source_code": this.encode(this.codeModel.value),
      "language_id": this.currentLanguageId,
      "number_of_runs": null,
      "stdin": this.encode(this.inputModel.value) || "",
      "expected_output": null,
      "cpu_time_limit": null,
      "cpu_extra_time": null,
      "wall_time_limit": null,
      "memory_limit": null,
      "stack_limit": null,
      "max_processes_and_or_threads": null,
      "enable_per_process_and_thread_time_limit": null,
      "enable_per_process_and_thread_memory_limit": null,
      "max_file_size": null,
      "enable_network": null
    }
    console.log(body);
    this.http.post("https://code-editor.deltanow.tech/submissions?base64_encoded=true&wait=true", body).subscribe((res: any) => {
      console.log(res);
      console.log(this.decode(res.stdout))
      console.log(this.decode(res.compile_output))
      this.compileError = this.decode(res.compile_output);
      if (res.status.id == 6) {
        this.outputModel = {
          language: 'plaintext',
          value: this.decode(res.compile_output),
          uri: 'main.txt'
        };
      } else if (res.status.id == 11) {
        this.outputModel = {
          language: 'plaintext',
          value: this.decode(res.stderr),
          uri: 'main.txt'
        };
      } else {
        this.outputModel = {
          language: 'plaintext',
          value: this.decode(res.stdout),
          uri: 'main.txt'
        }
      }
    },
      (err: any) => {
        console.log(err);
      })
  }

  sleep(ms: any) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  async getResult(token: any): Promise<any> {
    this.http.get("https://code-editor.deltanow.tech/submissions/" + token + "?base64_encoded=true").subscribe(async (res: any) => {
      console.log(res);
      if (res.status.id <= 2) {
        await this.sleep(2000);
        this.getResult(token);
      }
      this.compileError = this.decode(res.compile_output);
    },
      (err: any) => {
        console.log(err);
        this.compileError = this.decode(err["compile_output"]);
      })
  }


  decode(bytes: any) {
    var escaped = escape(atob(bytes || ""));
    try {
      return decodeURIComponent(escaped);
    } catch {
      return unescape(escaped);
    }
  }

  encode(str: any) {
    return btoa(unescape(encodeURIComponent(str || "")));
  }



}

export interface CodeModel {
  language: string;
  value: string;
  uri: string;

  dependencies?: Array<string>;
  schemas?: Array<{
    uri: string;
    schema: Object;
  }>;
}