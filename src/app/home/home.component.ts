import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CodeModel } from '@ngstack/code-editor';
import { CodeTemplatesService } from '../code-editor/code-templates.service';
import { NgxAgoraService, Stream, AgoraClient, ClientEvent, ClientConfig, StreamSpec, StreamEvent } from 'ngx-agora';
import { AngularFireDatabase } from '@angular/fire/database';
import { CloudRecordingService } from '../cloud-recording.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild('messageWindow') messageWindow: ElementRef | any;

  today: number = Date.now();
  currentMode = 'call';
  userType = 'candidate';
  fullScreenMode = false;

  theme = 'vs-dark';

  language: any;
  currentLanguageId: any;
  prevLanguageId: any;
  compileError: any;
  code: any;
  items: any;

  chatPopup = false;

  modes = [
    { id: 45, mode: 'UNKNOWN', uri: 'main.asm', name: 'Assembly (NASM 2.14.02)', template: this.codeTemplates.assemblySource },
    { id: 46, mode: 'shell', uri: 'main.sh', name: 'Bash (5.0.0)', template: this.codeTemplates.bashSource },
    { id: 47, mode: 'UNKNOWN', uri: 'main.bas', name: 'Basic (FBC 1.07.1)', template: this.codeTemplates.basicSource },
    // { id: 1011, mode: 'UNKNOWN', uri: 'main.bsq', name: 'Bosque (latest)', template: this.codeTemplates.bosqueSource },
    { id: 75, mode: 'c', uri: 'main.c', name: 'C (Clang 7.0.1)', template: this.codeTemplates.cSource },
    // { id: 1013, mode: 'c', uri: 'main.c', name: 'C (Clang 9.0.1)', template: this.codeTemplates.cSource },
    // { id: 1001, mode: 'c', uri: 'main.c', name: 'C (Clang 10.0.1)', template: this.codeTemplates.cSource },
    { id: 48, mode: 'c', uri: 'main.c', name: 'C (GCC 7.4.0)', template: this.codeTemplates.cSource },
    { id: 49, mode: 'c', uri: 'main.c', name: 'C (GCC 8.3.0)', template: this.codeTemplates.cSource },
    { id: 50, mode: 'c', uri: 'main.c', name: 'C (GCC 9.2.0)', template: this.codeTemplates.cSource },
    { id: 51, mode: 'csharp', uri: 'main.cs', name: 'C# (Mono 6.6.0.161)', template: this.codeTemplates.csharpSource },
    // { id: 1022, mode: 'csharp', uri: 'main.cs', name: 'C# (Mono 6.10.0.104)', template: this.codeTemplates.csharpSource },
    // { id: 1021, mode: 'csharp', uri: 'main.cs', name: 'C# (.NET Core SDK 3.1.302)', template: this.codeTemplates.csharpSource },
    // { id: 1023, mode: 'csharp', uri: 'main.cs', name: 'C# Test (.NET Core SDK 3.1.302, NUnit 3.12.0)', template: this.codeTemplates.csharpTestSource },
    { id: 76, mode: 'cpp', uri: 'main.cpp', name: 'C++ (Clang 7.0.1)', template: this.codeTemplates.cppSource },
    // { id: 1014, mode: 'cpp', uri: 'main.cpp', name: 'C++ (Clang 9.0.1)', template: this.codeTemplates.cppSource },
    // { id: 1002, mode: 'cpp', uri: 'main.cpp', name: 'C++ (Clang 10.0.1)', template: this.codeTemplates.cppSource },
    { id: 52, mode: 'cpp', uri: 'main.cpp', name: 'C++ (GCC 7.4.0)', template: this.codeTemplates.cppSource },
    { id: 53, mode: 'cpp', uri: 'main.cpp', name: 'C++ (GCC 8.3.0)', template: this.codeTemplates.cppSource },
    { id: 54, mode: 'cpp', uri: 'main.cpp', name: 'C++ (GCC 9.2.0)', template: this.codeTemplates.cppSource },
    { id: 60, mode: 'go', uri: 'main.go', name: 'Go (1.13.5)', template: this.codeTemplates.goSource },
    { id: 62, mode: 'java', uri: 'main.java', name: 'Java (OpenJDK 13.0.1)', template: this.codeTemplates.javaSource },
    // { id: 1004, mode: 'java', uri: 'main.java', name: 'Java (OpenJDK 14.0.1)', template: this.codeTemplates.javaSource },
    // { id: 1005, mode: 'java', uri: 'main.java', name: 'Java Test (OpenJDK 14.0.1, JUnit Platform Console Standalone 1.6.2)', template: this.codeTemplates.javaTestSource},
    { id: 63, mode: 'javascript', uri: 'main.js', name: 'JavaScript (Node.js 12.14.0)', template: this.codeTemplates.javaScriptSource },
    { id: 78, mode: 'kotlin', uri: 'main.kt', name: 'Kotlin (1.3.70)', template: this.codeTemplates.kotlinSource },
    { id: 67, mode: 'pascal', uri: 'main.pas', name: 'Pascal (FPC 3.0.4)', template: this.codeTemplates.pascalSource },
    { id: 85, mode: 'perl', uri: 'main.pl', name: 'Perl (5.28.1)', template: this.codeTemplates.perlSource },
    { id: 68, mode: 'php', uri: 'main.php', name: 'PHP (7.4.1)', template: this.codeTemplates.phpSource },
    { id: 43, mode: 'plaintext', uri: 'main.txt', name: 'Plain Text', template: this.codeTemplates.plainTextSource },
    { id: 70, mode: 'python', uri: 'main.py', name: 'Python (2.7.17)', template: this.codeTemplates.pythonSource },
    { id: 71, mode: 'python', uri: 'main.py', name: 'Python (3.8.1)', template: this.codeTemplates.pythonSource },
    // { id: 1010, mode: 'python', uri: 'main.py', name: 'Python for ML (3.7.3)', template: this.codeTemplates.pythonForMlSource },
    { id: 80, mode: 'r', uri: 'main.r', name: 'R (4.0.0)', template: this.codeTemplates.rSource },
    { id: 72, mode: 'ruby', uri: 'main.rb', name: 'Ruby (2.7.0)', template: this.codeTemplates.rubySource },
    { id: 73, mode: 'rust', uri: 'main.rs', name: 'Rust (1.40.0)', template: this.codeTemplates.rustSource },
    { id: 82, mode: 'sql', uri: 'main.sql', name: 'SQL (SQLite 3.27.2)', template: this.codeTemplates.sqliteSource },
    { id: 83, mode: 'swift', uri: 'main.swift', name: 'Swift (5.2.3)', template: this.codeTemplates.swiftSource },
    { id: 74, mode: 'typescript', uri: 'main.ts', name: 'TypeScript (3.7.4)', template: this.codeTemplates.typescriptSource },
    { id: 84, mode: 'vb', uri: 'main.vb', name: 'Visual Basic.Net (vbnc 0.0.0.5943)', template: this.codeTemplates.vbSource },
  ];

  codeModel: CodeModel = {
    language: 'c',
    uri: 'main.c',
    value: '',
    dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
  };

  outputModel: CodeModel = {
    language: 'plaintext',
    uri: 'main.txt',
    value: '',
    dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
  };

  inputModel: CodeModel = {
    language: 'plaintext',
    uri: 'output.txt',
    value: '',
    dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
  };

  options = {
    contextmenu: true,
    minimap: {
      enabled: false
    }
  };

  messages: Message | any = [];
  currentMessage = "";
  id: any;
  name: any = "";

  // Agora VC
  localCallId = 'agora_local';
  remoteCalls: string[] = [];
  remoteCallId: any;
  screenShareCallId: any;
  screenShareMode = false;
  private client: any;
  private screenClient: any;
  private screenStream: any;
  private localStream: any;
  private uid: any;
  private sid: any;
  screenShareBtn = false;
  audioTrack = true;
  videoTrack = true;

  constructor(public codeTemplates: CodeTemplatesService, public http: HttpClient, public db: AngularFireDatabase, public ngxAgoraService: NgxAgoraService, public cloudRecording: CloudRecordingService) {
    setInterval(() => { this.today = Date.now() }, 1);
    this.uid = Math.floor(Math.random() * 100);
    this.cloudRecording.uid = this.uid;
  }

  ngOnInit(): void {
    this.currentLanguageId = 50;
    this.prevLanguageId = 50;
    this.codeModel = {
      language: 'c',
      uri: 'main.c',
      value: this.codeTemplates.cSource,
      dependencies: ['@types/node', '@ngstack/translate', '@ngstack/code-editor']
    }
    this.id = Math.floor(Math.random() * 100);
    // this.name = prompt("Enter the name", "");
    this.name = "";
    this.getMessages();

    // Agora VC
    
    this.client = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
    // console.log(this.ngxAgoraService.AgoraRTC.Logger);
    this.ngxAgoraService.AgoraRTC.Logger.enableLogUpload();
    // this.ngxAgoraService.AgoraRTC.Logger.enableLogUpload();
    this.assignClientHandlers();
    this.localStream = this.ngxAgoraService.createStream({ streamID: this.uid, audio: true, video: true });
    this.localStream.setVideoProfile('720p_6');
    this.assignLocalStreamHandlers();
    this.initLocalStream(() => this.join(uid => this.publish(), error => console.log(error)));

    // this.cloudRecording.acquireResourceId();
  }

  changeMode(mode: any) {
    if (this.currentMode === 'screen-share' && this.screenShareBtn) {
      this.unpublishScreenShare();
    }
    this.currentMode = mode;
  }

  changeLanguage(): any {
    let x = this.modes.find((e) => e.id == this.currentLanguageId);
    let val = this.codeModel.value;
    if (x) {
      this.codeModel = {
        language: x.mode,
        uri: x.uri,
        value: x.template
      }
    }
    this.prevLanguageId = this.currentLanguageId;
    console.log(this.codeModel);
  }

  onCodeChanged(value: any) {
    this.code = value;
  }

  onLanguageChange(event: any): any {
    if (this.currentLanguageId !== this.prevLanguageId) {
      console.log("Hello")
      let y = this.modes.find((e) => e.id == this.prevLanguageId);
      if (y?.template !== this.codeModel.value) {
        if (confirm("The previous code will be erased. Do you wish to continue ?")) {
          this.changeLanguage();
        } else {
          console.log('dfghj')
          console.log(event.target.value);
          this.currentLanguageId = this.prevLanguageId;
          console.log(this.currentLanguageId);
        }
      } else {
        this.changeLanguage();
      }
    }
  }

  decode(bytes: any) {
    var escaped = escape(atob(bytes || ""));
    try {
      return decodeURIComponent(escaped);
    } catch {
      return unescape(escaped);
    }
  }

  encode(str: any) {
    return btoa(unescape(encodeURIComponent(str || "")));
  }

  compileCode(): any {
    let body = {
      "source_code": this.encode(this.codeModel.value),
      "language_id": this.currentLanguageId,
      "number_of_runs": null,
      "stdin": this.encode(this.inputModel.value) || "",
      "expected_output": null,
      "cpu_time_limit": null,
      "cpu_extra_time": null,
      "wall_time_limit": null,
      "memory_limit": null,
      "stack_limit": null,
      "max_processes_and_or_threads": null,
      "enable_per_process_and_thread_time_limit": null,
      "enable_per_process_and_thread_memory_limit": null,
      "max_file_size": null,
      "enable_network": null
    }
    console.log(body);
    this.http.post("https://code-editor.deltanow.tech/submissions?base64_encoded=true&wait=true", body).subscribe((res: any) => {
      console.log(res);
      console.log(this.decode(res.stdout))
      console.log(this.decode(res.compile_output))
      this.compileError = this.decode(res.compile_output);
      if (res.status.id == 6) {
        this.outputModel = {
          language: 'plaintext',
          value: this.decode(res.compile_output),
          uri: 'main.txt'
        };
      } else if (res.status.id == 11) {
        this.outputModel = {
          language: 'plaintext',
          value: this.decode(res.stderr),
          uri: 'main.txt'
        };
      } else {
        this.outputModel = {
          language: 'plaintext',
          value: this.decode(res.stdout),
          uri: 'main.txt'
        }
      }
    },
      (err: any) => {
        console.log(err);
      })
  }

  resetCode(): any {
    let x = this.modes.find((e) => e.id == this.currentLanguageId);
    if (x?.template !== this.codeModel.value) {
      if (confirm("The previous code will be erased. Do you wish to continue ?")) {
        if (x) {
          this.codeModel = {
            language: x.mode,
            uri: x.uri,
            value: x.template
          }
        }
      }
    } else {
      this.codeModel = {
        language: x.mode,
        uri: x.uri,
        value: x.template
      }
    }
  }

  toggleFullScreen(): any {
    if (this.fullScreenMode) {
      document.exitFullscreen();
    } else {
      let elem = document.documentElement;
      let methodToBeInvoked = elem.requestFullscreen;
      if (methodToBeInvoked) methodToBeInvoked.call(elem);
    }
    this.fullScreenMode = !this.fullScreenMode;
  }

  getMessages(): any {
    this.db.object('/rtm').valueChanges().subscribe((e: any) => {
      this.messages = JSON.parse(e);
      console.log(this.messages);
      console.log(e);
      this.scrollToBottom();
    })
  }

  sendMessage(): any {
    let messgObj: Message = {
      id: this.id,
      name: this.name,
      message: this.currentMessage,
      timeStamp: Date.now()
    }
    this.messages.push(messgObj);
    let messgStr = JSON.stringify(this.messages);
    this.db.object('/rtm').set(messgStr);
    this.currentMessage = "";
    this.scrollToBottom();
  }

  scrollToBottom(): any {
    try {
      this.messageWindow.nativeElement.scrollTop = this.messageWindow.nativeElement.scrollHeight;
    } catch (err) { }
  }

  screenShare(): void {
    this.screenClient = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });

    this.sid = new Date().getUTCMilliseconds();
    if (this.sid < 100) {
      this.sid += 100;
    }
    console.log(this.sid);
    this.screenClient.on(ClientEvent.LocalStreamPublished, (evt: any) => {
      console.log('Publish screen stream successfully');
    });

    this.screenStream = this.ngxAgoraService.createStream({ streamID: this.sid, audio: false, video: false, screen: true, screenAudio: false })
    this.screenStream.setVideoProfile('720p_6');
    this.assignScreenStreamHandlers();
    this.initScreenShare(() => this.joinScreenShare(uid => this.publishScreenShare(), error => console.log(error)));
  }

  join(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
    console.log("Hellllllllllllll")
    this.http.get('http://localhost:3000/generate/token?uid='+this.uid).subscribe((res: any) => {
      console.log(res);
      this.client.join(res.token, '007', this.uid, onSuccess, onFailure);
    })
    // this.client.join('006e79d64022bde4fb8a5891c994bd0c705IAC2Dt47fnSKrmH5XN+OKJzu64rX4YhmLicCDP5i75ZAIX6v0bxXoFHlIgCiqMf1y+gCYQQAAQDFhThhAgDFhThhAwDFhThhBADFhThh', '007', this.uid, onSuccess, onFailure);
  }

  joinScreenShare(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
    this.screenClient.join('006e79d64022bde4fb8a5891c994bd0c705IAC2Dt47fnSKrmH5XN+OKJzu64rX4YhmLicCDP5i75ZAIX6v0bxXoFHlIgCiqMf1y+gCYQQAAQDFhThhAgDFhThhAwDFhThhBADFhThh', '007', this.sid, onSuccess, onFailure);
  }

  publishScreenShare(): void {
    this.screenClient.publish(this.screenStream, (err: any) => console.log('Publish screen stream error: ' + err));
    this.screenShareBtn = true;
  }

  publish(): void {
    this.client.publish(this.localStream, (err: any) => console.log('Publish local stream error: ' + err));
  }

  unpublishScreenShare(): void {
    this.screenClient.unpublish(this.screenStream, (err: any) => console.log('Publish screen stream error: ' + err));
    this.screenShareBtn = false;
  }

  private assignClientHandlers(): void {
    this.client.on(ClientEvent.LocalStreamPublished, (evt: any) => {
      console.log('Publish local stream successfully');
    });

    this.client.on(ClientEvent.Error, (error: any) => {
      console.log('Got error msg:', error.reason);
      if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.client.renewChannelKey(
          '',
          () => console.log('Renewed the channel key successfully.'),
          (renewError: any) => console.error('Renew channel key failed: ', renewError)
        );
      }
    });


    this.client.on(ClientEvent.RemoteStreamAdded, (evt: any) => {
      console.log("New User Joined");
      const stream = evt.stream as Stream;
      this.client.subscribe(stream, (err: any) => {
        console.log('Subscribe stream failed', err);
      });
    });


    this.client.on(ClientEvent.RemoteStreamSubscribed, (evt: any) => {
      const stream = evt.stream as Stream;
      console.log(stream);
      const id = this.getRemoteId(stream);
      console.log(id);
      console.log("From remote client");
      this.remoteCalls.push(id);
      const streamContain = "video" + stream.getId();
      const playerRadius = "player_" + stream.getId();
      if (stream.getId() > 100) {
        this.currentMode = 'screen-share';
        this.screenShareCallId = id;
        this.screenShareMode = true;
      } else {
        this.remoteCallId = id;
      }
      console.log(streamContain)
      console.log(playerRadius)
      console.log(this.remoteCalls);
      setTimeout(() => { 
        stream.play(id);
        let x = document.getElementById(streamContain);
        if (typeof x !== 'undefined' && x !== null && x.style && x.style.objectFit) {
          x.style.objectFit = 'contain';
        }
        let y = document.getElementById(playerRadius);
        if (y && y.style) {
          console.log("asdfghmsdfg")
          y.style.borderRadius = '5px';
        }
      }, 1000);
      console.log(this.remoteCalls)
    });



    this.client.on(ClientEvent.RemoteStreamRemoved, (evt: any) => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        console.log(this.remoteCalls)
        var clientName = "agora_remote-" + stream.getId();
        if (stream.getId() > 100) {
          this.currentMode = 'call';
          this.screenShareMode = false;
        }
        this.remoteCalls = this.remoteCalls.filter(function (val, index, arr) {
          return val != clientName
        })
        console.log(`Remote stream is removed ${stream.getId()}`);
      }
    });


    this.client.on(ClientEvent.PeerLeave, (evt: any) => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        if (stream.getId() > 100) {
          this.currentMode = 'call';
          this.screenShareMode = false;
        }
        this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
        console.log(`${evt.uid} left from this channel`);
      }
    });

  }

  private assignLocalStreamHandlers(): void {
    this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
    });

    // The user has denied access to the camera and mic.
    this.localStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
    });
  }

  private assignScreenStreamHandlers(): void {
    // The user has denied access to the camera and mic.
    this.screenStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
    });

    this.screenStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
    });
  }

  private initLocalStream(onSuccess?: () => any): void {
    this.localStream.init(
      () => {
        // The user has granted access to the camera and mic.
        this.localStream.play(this.localCallId);
        const streamContain = "video" + this.uid
        const playerRadius = "player_" + this.uid;
        let x = document.getElementById(streamContain);
        if (typeof x !== 'undefined' && x !== null && x.style && x.style.objectFit) {
          x.style.objectFit = 'contain';
        }
        let y = document.getElementById(playerRadius);
        if (y && y.style) {
          y.style.borderRadius = '5px';
        }
        if (onSuccess) {
          onSuccess();
        }
      },
      (err: any) => console.error('getUserMedia failed', err)
    );

  }

  public initScreenShare(onSuccess?: () => any): void {
    this.screenStream.init(
      () => {
        // The user has granted access to the camera and mic.
        if (onSuccess) {
          onSuccess();
        }
      },
      (err: any) => console.error('getUserMedia failed', err)
    );
  }

  private getRemoteId(stream: Stream): string {
    return `agora_remote-${stream.getId()}`;
  }

  public toggleMic(): any {
    if (this.audioTrack) {
      this.localStream.muteAudio();
    } else {
      this.localStream.unmuteAudio();
    }
    this.audioTrack = !this.audioTrack;
  }

  public toggleVideo(): any {
    if (this.videoTrack) {
      this.localStream.muteVideo();
    } else {
      this.localStream.unmuteVideo();
    }
    this.videoTrack = !this.videoTrack;
  }

  public disableScreenShareBtn(): any {
    if (this.screenShareMode) {
      if (this.screenShareBtn) {
        return false;
      } else {
        return true;
      } 
    }
    return false;
  }

  public endCall(): any {
    top.close();
  }
}

export class Message {
  id: any;
  name: String | any;
  message: string | any;
  timeStamp: any;
}